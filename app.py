from flask import request
from marshmallow import fields, ValidationError, Schema
from flask_restful import Resource, Api
from thread import process
from model import create_app,db

app = create_app()

api = Api(app)

def validate_item(item):
    items = ['book', 'pen', 'folder', 'bag']
    if item not in items:
        raise ValidationError("Item cannot be accepted")

class items(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    status = db.Column(db.VARCHAR(20))
    item = db.Column(db.VARCHAR(20))

class ItemsSchema(Schema):
    item = fields.String(validate=validate_item)

class GetMethod(Resource):
    def get(self,delay_value):
        duration = process(delay_value)
        return {'time_taken' : duration},200

class PostMethod(Resource):
    def post(self):
        item = request.get_json()
        val = item['item']
        try:
            result = ItemsSchema().load(item)
            data = items(item=val,status="pending")
            db.session.add(data)
            db.session.commit()
            return {'item': data.item, 'status' : data.status, 'id' : data.id},200
        except ValidationError as err:
            return {'error' : err.messages['item'] },400


api.add_resource(GetMethod, '/delay/<int:delay_value>')
api.add_resource(PostMethod, '/add')


if __name__ == '__main__':
    app.run(debug=True)