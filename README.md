# rest-api

Steps to run the project:

1.  Go to root folder
2.  python app.py


Files included :

1. requirement.txt                          -- required package
2. README.md                                -- Instructions
3. Assignment.postman_collection.json       -- Postman Collection
